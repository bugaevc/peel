# GLib

skip	g_strfreev
skip	g_strjoinv
skip	g_strv_contains
skip	g_strv_equal
skip	g_strv_length
skip	g_vasprintf

skip	g_ascii_dtostr
skip	g_ascii_strtod
skip	g_ascii_strtoll
skip	g_ascii_strtoull
skip	g_ascii_formatd
skip	g_assertion_message_cmpstrv
skip	g_atomic_int_add
skip	g_atomic_int_and
skip	g_atomic_int_compare_and_exchange
skip	g_atomic_int_compare_and_exchange_full
skip	g_atomic_int_dec_and_test
skip	g_atomic_int_exchange
skip	g_atomic_int_exchange_and_add
skip	g_atomic_int_get
skip	g_atomic_int_inc
skip	g_atomic_int_or
skip	g_atomic_int_set
skip	g_atomic_int_xor
skip	g_atomic_pointer_add
skip	g_atomic_pointer_and
skip	g_atomic_pointer_compare_and_exchange
skip	g_atomic_pointer_compare_and_exchange_full
skip	g_atomic_pointer_exchange
skip	g_atomic_pointer_get
skip	g_atomic_pointer_or
skip	g_atomic_pointer_set
skip	g_atomic_pointer_xor
skip	g_atomic_rc_box_dup
skip	g_atomic_ref_count_compare
skip	g_atomic_ref_count_dec
skip	g_atomic_ref_count_inc
skip	g_atomic_ref_count_init
skip	g_base64_decode_inplace
skip	g_bit_lock
skip	g_bit_trylock
skip	g_bit_unlock
skip	g_clear_handle_id
skip	g_clear_signal_handler
skip	g_completion_complete
skip	g_completion_complete_utf8
skip	g_date_strftime
skip	g_freopen
skip	g_hmac_new
skip	g_hmac_get_digest
skip	g_hmac_update
skip	g_iconv
skip	g_log_writer_default_set_debug_domains
skip	g_io_channel_read
skip	g_io_channel_read_line_string
skip	g_io_channel_write
out	g_io_channel_get_line_term	length
skip	g_slice_get_config_state
skip	g_main_context_pusher_new
skip	g_main_context_pusher_free
skip	g_mkdtemp
skip	g_mkdtemp_full
skip	g_mkstemp
skip	g_mkstemp_full
skip	g_once_init_enter
skip	g_once_init_enter_impl
skip	g_once_init_enter_pointer
skip	g_once_init_leave
skip	g_once_init_leave_pointer
skip	g_pointer_bit_lock
skip	g_pointer_bit_trylock
skip	g_pointer_bit_unlock
skip	g_pointer_bit_lock_and_get
skip	g_pointer_bit_unlock_and_set
skip	g_prefix_error_literal
skip	g_rand_new_with_seed_array
skip	g_rand_set_seed_array
skip	g_rc_box_dup
skip	g_ref_count_compare
skip	g_ref_count_dec
skip	g_ref_count_inc
skip	g_ref_count_init
skip	g_ref_string_acquire
skip	g_ref_string_length
skip	g_ref_string_release
skip	g_set_prgname_once
skip	g_strtod
skip	g_stpcpy
skip	g_strcanon
skip	g_strchomp
skip	g_strchug
skip	g_strdelimit
skip	g_strdown
skip	g_strlcat
skip	g_strlcpy
skip	g_strreverse
skip	g_strup
skip	g_test_log_buffer_push
skip	g_timer_elapsed
skip	g_type_class_adjust_private_offset
skip	g_unicode_canonical_decomposition
skip	g_unicode_canonical_ordering
skip	g_utf8_strncpy
skip	g_utime
owned	g_variant_new_take_string	string
skip	g_vsnprintf
skip	g_vsprintf
skip	g_tree_search
skip	g_tree_search_node
scope	g_timeout_add			function	GSourceFunc
scope	g_timeout_add_once		function	async
scope	g_timeout_add_seconds		function	GSourceFunc
scope	g_timeout_add_seconds_once	function	async
scope	g_idle_add			function	GSourceFunc
scope	g_idle_add_once			function	async
scope	g_io_add_watch			func		GSourceFunc
scope	g_unix_signal_add		handler		GSourceFunc
scope	g_unix_fd_add			function	GSourceFunc
scope	g_main_context_invoke		function	GSourceFunc
skip	g_datalist_id_replace_data
skip	g_ucs4_to_utf8
skip	g_ucs4_to_utf16
skip	g_unichar_to_utf8
skip	g_utf16_to_ucs4
skip	g_utf8_to_ucs4
skip	g_utf8_to_ucs4_fast
skip	g_utf8_to_utf16
skip	g_utf16_to_utf8
out	g_unichar_get_mirror_char	mirrored_ch
skip	GUnixPipeEnd
skip	GUnixPipe
skip	GHook
skip	GHookList
unowned	g_variant_type_string_scan	endptr
unowned	g_bytes_new_static		data
unowned	g_mapped_file_get_contents	return
no-autoref GHook
no-autofree GArray
no-autofree GByteArray
no-autofree GHook
no-autofree GKeyFile
no-autofree GMappedFile
no-autofree GMarkupParseContext
no-autofree GMatchInfo
no-autofree GMemChunk
no-autofree GOptionGroup
no-autofree GPtrArray
no-autofree GThreadPool
include	g_access			glib/gstdio.h
include	g_chmod				glib/gstdio.h
include	g_open				glib/gstdio.h
include	g_creat				glib/gstdio.h
float	GVariant
#ref-sink GVariant			g_variant_ref_sink
nest	GVariantType			Variant::Type
nest	GVariantClass			Variant::Class
nest	GVariantIter			Variant::Iter
nest	GVariantBuilder			Variant::Builder
nest	GMainContextFlags		MainContext::Flags
onstack	GTimeVal
onstack	GDate
onstack	GLogField
onstack	GPollFD
opaque	GSource

onstack	GHashTableIter
opaque	GHashTableIter
nest	GHashTableIter			HashTable::Iter

# These are deprecated, and don't work yet since GStaticMutex is a union
skip	GStaticMutex
skip	GStaticRecMutex
skip	GStaticRWLock
skip	GStaticPrivate

# Should have explicit bindings
skip	GVariant			GLib		manual

inout	g_time_zone_adjust_time		time_

vararg	g_error_new			format
vararg	g_set_error			format
vararg	g_string_printf			format
vararg	g_string_append_printf		format

vararg	g_log				format
vararg	g_prefix_error			format
vararg	g_print				format
vararg	g_printerr			format
#vararg	printf				format
#vararg	fprintf				format

# Not actually argv, but close enough:
vararg	g_build_filename		argv
vararg	g_build_path			argv

#ifdef	GUnixFDSourceFunc		G_OS_UNIX
#ifdef	GUnixPipe			G_OS_UNIX
#ifdef	GUnixPipeEnd			G_OS_UNIX
#ifdef	g_closefrom			G_OS_UNIX
#ifdef	g_unix_error_quark		G_OS_UNIX
#ifdef	g_unix_fd_add			G_OS_UNIX
#ifdef	g_unix_fd_add_full		G_OS_UNIX
#ifdef	g_unix_fd_source_new		G_OS_UNIX
#ifdef	g_fdwalk_set_cloexec		G_OS_UNIX
#ifdef	g_unix_get_passwd_entry		G_OS_UNIX
#ifdef	g_unix_open_pipe		G_OS_UNIX
#ifdef	g_unix_set_fd_nonblocking	G_OS_UNIX
#ifdef	g_unix_signal_add		G_OS_UNIX
#ifdef	g_unix_signal_add_full		G_OS_UNIX
#ifdef	g_unix_signal_source_new	G_OS_UNIX
skip	GUnixFDSourceFunc
skip	GUnixPipe
skip	GUnixPipeEnd
skip	g_closefrom
skip	g_unix_error_quark
skip	g_unix_fd_add
skip	g_unix_fd_add_full
skip	g_unix_fd_source_new
skip	g_fdwalk_set_cloexec
skip	g_unix_get_passwd_entry
skip	g_unix_open_pipe
skip	g_unix_set_fd_nonblocking
skip	g_unix_signal_add
skip	g_unix_signal_add_full
skip	g_unix_signal_source_new
ifdef	g_source_add_unix_fd		G_OS_UNIX
ifdef	g_source_modify_unix_fd		G_OS_UNIX
ifdef	g_source_remove_unix_fd		G_OS_UNIX
ifdef	g_source_query_unix_fd		G_OS_UNIX

# This one is a fun hack:
no-autofree GString
free	GString				[](::GString*str){g_string_free(str,TRUE);}
skip	g_string_free

# GObject
# These all have (or should have) explicit bindings.
skip	GType				GObject		manual
skip	GObject				GObject		manual
ref	GObject				g_object_ref
unref	GObject				g_object_unref
ref-sink GObject			g_object_ref_sink
skip	GTypeInstance			GObject		manual
skip	GTypeClass			GObject		manual
skip	GTypeInterface			GObject		manual
type-struct-for GTypeClass		GObject.TypeInstance
# The following line is a lie, but close enough.
type-struct	GTypeInstance		GObject.TypeClass
nest	GObjectConstructParam		Object::ConstructParam
skip	GObjectConstructParam		GObject		manual
nest-alias GBindingFlags		Binding::Flags
skip	GValue				GObject		manual
onstack	GValue
float	GInitiallyUnowned
skip	g_gtype_get_type
skip	g_type_check_instance
skip	g_type_check_instance_cast
skip	g_type_check_instance_is_a
skip	g_type_check_instance_is_fundamentally_a
skip	g_type_check_is_value_type
skip	g_type_check_value
skip	g_type_check_value_holds
skip	g_type_create_instance
skip	g_type_depth
skip	g_type_ensure
skip	g_type_from_name
skip	g_type_fundamental
skip	g_type_fundamental_next
skip	g_type_get_instance_count
skip	g_type_init
skip	g_type_is_a
skip	g_type_name
skip	g_type_name_from_class
skip	g_type_name_from_instance
skip	g_type_parent
skip	g_type_class_peek
skip	g_type_free_instance
skip	g_type_next_base
skip	g_type_test_flags
skip	g_type_check_class_cast
skip	g_type_check_class_is_a

skip	g_type_interface_peek
skip	g_closure_add_finalize_notifier
skip	g_closure_add_invalidate_notifier
skip	g_closure_add_marshal_guards
skip	g_closure_set_meta_marshal
skip	g_signal_group_connect
skip	g_signal_group_connect_after
skip	g_signal_group_connect_swapped
skip	g_signal_handler_find
skip	g_signal_handlers_block_matched
skip	g_signal_handlers_disconnect_matched
skip	g_signal_handlers_unblock_matched

float	GClosure
float	g_source_set_closure		closure
float	g_binding_group_bind_with_closures transform_to
float	g_binding_group_bind_with_closures transform_from

ref	GParamSpec			g_param_spec_ref
ref-sink GParamSpec			g_param_spec_ref_sink
float	GParamSpec
take-value GParamSpec			g_value_take_param

onstack	GSignalQuery
onstack	GTypeQuery

# Gio
skip	g_buffered_input_stream_peek_buffer
skip	g_networking_init
skip	g_inet_address_to_bytes
skip	g_keyfile_settings_backend_new
skip	g_memory_settings_backend_new
skip	g_null_settings_backend_new
skip	g_cancellable_connect
skip	g_vfs_register_uri_scheme
skip	g_file_copy_async
skip	GFile::vfunc_copy_async
skip	GDBusProxy::vfunc_g_properties_changed
skip	GDBusObjectManagerClient::vfunc_interface_proxy_properties_changed
skip	GInetAddress::vfunc_to_bytes
skip	GSettings::vfunc_change_event
skip	GVfs::vfunc_local_file_add_info
skip	GThreadedResolver
ifdef	GSettingsBackend		G_SETTINGS_ENABLE_BACKEND

in	g_socket_receive		size
in	g_socket_receive_from		size
in	g_socket_receive_with_blocking	size

float	g_file_copy_async_with_closures	progress_callback_closure
float	g_file_copy_async_with_closures	ready_callback_closure
float	g_file_move_async_with_closures	progress_callback_closure
float	g_file_move_async_with_closures	ready_callback_closure
float	g_dbus_gvariant_to_gvalue	value
float	g_bus_own_name_with_closures	bus_acquired_closure
float	g_bus_own_name_with_closures	name_acquired_closure
float	g_bus_own_name_with_closures	name_lost_closure
float	g_bus_own_name_on_connection_with_closures	name_acquired_closure
float	g_bus_own_name_on_connection_with_closures	name_lost_closure
float	g_dbus_connection_register_object_with_closures	method_call_closure
float	g_dbus_connection_register_object_with_closures	get_property_closure
float	g_dbus_connection_register_object_with_closures	set_property_closure
float	g_settings_bind_with_mapping_closures	get_mapping
float	g_settings_bind_with_mapping_closures	set_mapping

float	g_action_change_state		value
float	GAction::vfunc_change_state	value
float	g_action_activate		parameter
float	GAction::vfunc_activate		parameter
float	g_action_group_activate_action	parameter
float	GActionGroup::vfunc_activate_action	parameter
float	g_action_group_change_action_state	value
float	GActionGroup::vfunc_change_action_state	value
float	g_simple_action_new_stateful	state
float	g_simple_action_set_state	value
float	g_menu_item_set_attribute_value	value
float	g_menu_item_set_action_and_target_value			target_value
float	g_notification_add_button_with_target_value		target
float	g_notification_set_default_action_and_target_value	target

vararg	g_simple_async_report_error_in_idle	format
vararg	g_application_command_line_print	format
vararg	g_application_command_line_printerr	format
vararg	g_dbus_method_invocation_return_error	format
vararg	g_dbus_message_new_method_error	format
vararg	g_task_report_new_error		format
vararg	g_task_return_new_error		format
vararg	g_task_return_prefixed_error	format
vararg	g_simple_async_result_new_error	format
vararg	g_simple_async_result_set_error	format
owned	g_simple_async_result_new_take_error	error
vararg	g_subprocess_launcher_spawn	argv
owned	g_subprocess_launcher_spawn	error
out	g_subprocess_launcher_spawn	error
optional g_subprocess_launcher_spawn	error
vararg	g_subprocess_new		argv
owned	g_subprocess_new		error
out	g_subprocess_new		error
optional g_subprocess_new		error
# Not actually argv, but close enough:
vararg	g_file_new_build_filename	argv
vararg	g_settings_get			variant-get
vararg	g_settings_set			variant-new
vararg	g_output_stream_printf		format
owned	g_output_stream_printf		error
out	g_output_stream_printf		error
optional g_output_stream_printf		error
owned	g_output_stream_vprintf		error
out	g_output_stream_vprintf		error
optional g_output_stream_vprintf	error
vararg	g_menu_item_get_attribute	variant-get
vararg	g_menu_item_set_attribute	variant-new
vararg	g_menu_model_get_item_attribute	variant-get
vararg	g_menu_item_set_action_and_target	variant-new
vararg	g_notification_add_button_with_target	variant-new
vararg	g_notification_set_default_action_and_target variant-new
vararg	g_initable_new			object-new
vararg	g_async_initable_new_async	object-new

onstack	GInputVector
onstack	GOutputVector
onstack	GInputMessage
onstack	GOutputMessage
onstack	GActionEntry

nest	GApplicationFlags		Application::Flags
nest	GTlsPasswordFlags		TlsPassword::Flags
nest	GTlsCertificateFlags		TlsCertificate::Flags
nest	GTlsCertificateRequestFlags	TlsCertificate::RequestFlags
nest	GTlsDatabaseVerifyFlags		TlsDatabase::VerifyFlags
nest	GTlsDatabaseLookupFlags		TlsDatabase::LookupFlags
nest	GResourceLookupFlags		Resource::LookupFlags
nest	GResourceFlags			Resource::Flags
nest	GResourceError			Resource::Error
nest	GFileAttributeType		File::AttributeType
nest	GFileAttributeInfoFlags		File::AttributeInfoFlags
nest	GFileCopyFlags			File::CopyFlags
nest	GFileCreateFlags		File::CreateFlags
nest	GFileMeasureFlags		File::MeasureFlags
nest	GFileMonitorFlags		File::MonitorFlags
nest	GFileQueryInfoFlags		File::QueryInfoFlags
nest	GTestDBusFlags			TestDBus::Flags
nest	GConverterFlags			Converter::Flags
nest	GConverterResult		Converter::Result
nest	GDBusConnectionFlags		DBusConnection::Flags
nest	GDBusProxyFlags			DBusProxy::Flags
nest	GDBusServerFlags		DBusServer::Flags
nest	GDBusMessageFlags		DBusMessage::Flags
nest	GDBusMessageType		DBusMessage::Type
nest	GDBusMessageByteOrder		DBusMessage::ByteOrder
nest	GDBusMessageHeaderField		DBusMessage::HeaderField
nest	GDBusPropertyInfoFlags		DBusPropertyInfo::Flags
nest	GDBusInterfaceSkeletonFlags	DBusInterfaceSkeleton::Flags
nest	GDBusInterfaceInfo		DBusInterface::Info
nest	GDBusInterfaceVTable		DBusInterface::VTable
nest	GDBusObjectManagerClientFlags	DBusObjectManagerClient::Flags
nest	GDriveStartFlags		Drive::StartFlags
nest	GDriveStartStopType		Drive::StartStopType
nest	GMountMountFlags		Mount::MountFlags
nest	GMountUnmountFlags		Mount::UnmountFlags
nest	GSubprocessFlags		Subprocess::Flags
nest	GSettingsBindFlags		Settings::BindFlags
nest	GAppInfoCreateFlags		AppInfo::CreateFlags
nest	GIOModuleScope			IOModule::Scope
#nest	GIOModuleScopeFlags		IOModule::Scope::Flags
nest	GIOModuleScopeFlags		IOModule::ScopeFlags
nest	GIOStreamSpliceFlags		IOStream::SpliceFlags
nest	GOutputStreamSpliceFlags	OutputStream::SpliceFlags
nest	GResolverNameLookupFlags	Resolver::NameLookupFlags


no-autobase GInitable
no-autobase GAsyncInitable
prefer-autobase GActionGroup		ActionMap

float	g_dbus_proxy_call		parameters
float	g_dbus_proxy_call_sync		parameters
float	g_dbus_proxy_call_with_unix_fd_list		parameters
float	g_dbus_proxy_call_with_unix_fd_list_sync	parameters
float	g_dbus_connection_call		parameters
float	g_dbus_connection_call_sync	parameters
float	g_dbus_connection_call_with_unix_fd_list	parameters
float	g_dbus_connection_call_with_unix_fd_list_sync	parameters
float	g_dbus_connection_emit_signal	parameters
float	g_dbus_proxy_set_cached_property value
float	g_dbus_message_set_body		body
float	g_dbus_message_set_header	value
float	g_dbus_method_invocation_return_value		parameters
float	g_dbus_method_invocation_return_value_with_unix_fd_list parameters

cpp-wrapper GTaskThreadFunc

# For compatibility reasons, these APIs are present in Gio-2.0.gir,
# as well as in newer GioUnix-2.0.gir. We don't want to put them into
# peel::Gio bindings, so skip them when generating that.
repo-skip gio-unix-2.0			Gio
repo-skip gio/gdesktopappinfo.h		Gio
repo-skip gio/gfiledescriptorbased.h	Gio
repo-skip gio/gunixfdmessage.h		Gio
repo-skip gio/gunixinputstream.h	Gio
repo-skip gio/gunixmounts.h		Gio
repo-skip gio/gunixoutputstream.h	Gio
skip	GDesktopAppInfo			Gio
skip	GDesktopAppInfoLookup		Gio
skip	GDesktopAppLaunchCallback	Gio
skip	GUnixFDMessage			Gio
skip	GFileDescriptorBased		Gio
skip	GUnixInputStream		Gio
skip	GUnixMountMonitor		Gio
skip	GUnixMountPoint			Gio
skip	GUnixMountEntry			Gio
skip	GUnixOutputStream		Gio
skip	g_unix_is_mount_path_system_internal Gio
skip	g_unix_is_system_device_path	Gio
skip	g_unix_is_system_fs_type	Gio
skip	g_unix_mount_at			Gio
skip	g_unix_mount_compare		Gio
skip	g_unix_mount_copy		Gio
skip	g_unix_mount_for		Gio
skip	g_unix_mount_free		Gio
skip	g_unix_mount_get_device_path	Gio
skip	g_unix_mount_get_fs_type	Gio
skip	g_unix_mount_get_mount_path	Gio
skip	g_unix_mount_get_options	Gio
skip	g_unix_mount_get_root_path	Gio
skip	g_unix_mount_guess_can_eject	Gio
skip	g_unix_mount_guess_icon		Gio
skip	g_unix_mount_guess_name		Gio
skip	g_unix_mount_guess_should_display Gio
skip	g_unix_mount_guess_symbolic_icon Gio
skip	g_unix_mount_is_readonly	Gio
skip	g_unix_mount_is_system_internal	Gio
skip	g_unix_mount_point_at		Gio
skip	g_unix_mount_point_compare	Gio
skip	g_unix_mount_point_copy		Gio
skip	g_unix_mount_point_free		Gio
skip	g_unix_mount_point_get_device_path Gio
skip	g_unix_mount_point_get_fs_type	Gio
skip	g_unix_mount_point_get_mount_path Gio
skip	g_unix_mount_point_get_options	Gio
skip	g_unix_mount_point_guess_can_eject Gio
skip	g_unix_mount_point_guess_icon	Gio
skip	g_unix_mount_point_guess_name	Gio
skip	g_unix_mount_point_guess_symbolic_icon Gio
skip	g_unix_mount_point_is_loopback	Gio
skip	g_unix_mount_point_is_readonly	Gio
skip	g_unix_mount_point_is_user_mountable Gio
skip	g_unix_mount_points_changed_since Gio
skip	g_unix_mount_points_get		Gio
skip	g_unix_mount_points_get_from_file Gio
skip	g_unix_mounts_changed_since	Gio
skip	g_unix_mounts_get		Gio
skip	g_unix_mounts_get_from_file	Gio

ifdef	g_dbus_message_get_unix_fd_list	G_OS_UNIX
ifdef	g_dbus_message_set_unix_fd_list	G_OS_UNIX
ifdef	g_credentials_get_unix_pid	G_OS_UNIX
ifdef	g_credentials_get_unix_user	G_OS_UNIX
ifdef	g_credentials_set_unix_user	G_OS_UNIX
ifdef	g_subprocess_send_signal	G_OS_UNIX
ifdef	g_subprocess_launcher_take_fd	G_OS_UNIX
ifdef	g_subprocess_launcher_close	G_OS_UNIX
ifdef	g_subprocess_launcher_set_stdin_file_path	G_OS_UNIX
ifdef	g_subprocess_launcher_take_stdin_fd		G_OS_UNIX
ifdef	g_subprocess_launcher_set_stdout_file_path	G_OS_UNIX
ifdef	g_subprocess_launcher_take_stdout_fd		G_OS_UNIX
ifdef	g_subprocess_launcher_set_stderr_file_path	G_OS_UNIX
ifdef	g_subprocess_launcher_take_stderr_fd		G_OS_UNIX
ifdef	g_subprocess_launcher_set_child_setup		G_OS_UNIX
ifdef	g_dbus_connection_call_with_unix_fd_list	G_OS_UNIX
ifdef	g_dbus_connection_call_with_unix_fd_list_finish	G_OS_UNIX
ifdef	g_dbus_connection_call_with_unix_fd_list_sync	G_OS_UNIX
ifdef	g_dbus_method_invocation_return_value_with_unix_fd_list G_OS_UNIX
ifdef	g_dbus_proxy_call_with_unix_fd_list		G_OS_UNIX
ifdef	g_dbus_proxy_call_with_unix_fd_list_finish	G_OS_UNIX
ifdef	g_dbus_proxy_call_with_unix_fd_list_sync	G_OS_UNIX

skip	g_registry_settings_backend_new	Gio
skip	GWin32InputStream		Gio
skip	GWin32OutputStream		Gio
skip	GWin32NetworkMonitor		Gio
opaque	GWin32NetworkMonitor
skip	GWin32NetworkMonitorClass	Gio
repo-skip gio-windows-2.0		Gio
repo-skip gio/gwin32inputstream.h	Gio
repo-skip gio/gwin32outputstream.h	Gio
repo-skip gio/gregistrysettingsbackend.h Gio

# cairo
raw	cairo
#skip	cairo_image_surface_create
nest	cairo_surface_type_t		Surface::Type

# xlib
raw	xlib

# freetype2
raw	freetype2

# HarfBuzz
raw	HarfBuzz
#skip	hb_aat_layout_feature_selector_info_t
#skip	hb_aat_layout_feature_selector_t
#skip	hb_aat_layout_feature_type_get_name_id
#skip	hb_aat_layout_feature_type_t
#skip	hb_aat_layout_has_positioning
#skip	hb_aat_layout_has_substitution
#skip	hb_aat_layout_has_tracking
#skip	hb_buffer_get_language
#skip	hb_font_set_funcs
#skip	hb_font_set_funcs_data
#skip	hb_ft_face_create
#skip	hb_ft_face_create_cached
#skip	hb_ft_face_create_referenced
#skip	hb_ft_font_changed
#skip	hb_ft_font_create
#skip	hb_ft_font_create_referenced
#skip	hb_ft_font_get_face
#skip	hb_ft_font_get_load_flags
#skip	hb_ft_font_lock_face
#skip	hb_ft_font_set_funcs
#skip	hb_ft_font_set_load_flags
#skip	hb_ft_font_unlock_face
#skip	hb_ft_hb_font_changed
#skip	hb_glib_blob_create
#skip	hb_glib_get_unicode_funcs
#skip	hb_glib_script_from_script
#skip	hb_glib_script_to_script
#skip	hb_language_get_default
#skip	hb_ot_color_glyph_has_paint
#skip	hb_ot_color_has_layers
#skip	hb_ot_color_has_paint
#skip	hb_ot_color_has_palettes
#skip	hb_ot_color_has_png
#skip	hb_ot_color_has_svg
#skip	hb_ot_color_layer_t
#skip	hb_ot_color_palette_color_get_name_id
#skip	hb_ot_color_palette_get_count
#skip	hb_ot_color_palette_get_name_id
#skip	hb_ot_font_set_funcs
#skip	hb_ot_layout_baseline_tag_t
#skip	hb_ot_layout_get_glyph_class
#skip	hb_ot_layout_get_horizontal_baseline_tag_for_script
#skip	hb_ot_layout_glyph_class_t
#skip	hb_ot_layout_has_glyph_classes
#skip	hb_ot_layout_has_positioning
#skip	hb_ot_layout_has_substitution
#skip	hb_ot_layout_lookup_get_optical_bound
#skip	hb_ot_layout_lookup_would_substitute
#skip	hb_ot_layout_script_find_language
#skip	hb_ot_layout_table_get_lookup_count
#skip	hb_ot_math_constant_t
#skip	hb_ot_math_get_constant
#skip	hb_ot_math_get_glyph_italics_correction
#skip	hb_ot_math_get_glyph_kerning
#skip	hb_ot_math_get_glyph_top_accent_attachment
#skip	hb_ot_math_get_min_connector_overlap
#skip	hb_ot_math_glyph_part_t
#skip	hb_ot_math_glyph_variant_t
#skip	hb_ot_math_has_data
#skip	hb_ot_math_is_glyph_extended_shape
#skip	hb_ot_math_kern_entry_t
#skip	hb_ot_math_kern_t
#skip	hb_ot_meta_reference_entry
#skip	hb_ot_meta_tag_t
#skip	hb_ot_metrics_get_variation
#skip	hb_ot_metrics_get_x_variation
#skip	hb_ot_metrics_get_y_variation
#skip	hb_ot_metrics_tag_t
#skip	hb_ot_name_entry_t
#skip	hb_ot_name_id_predefined_t
#skip	hb_ot_tag_from_language
#skip	hb_ot_tag_to_language
#skip	hb_ot_tag_to_script
#skip	hb_ot_var_axis_info_t
#skip	hb_ot_var_axis_t
#skip	hb_ot_var_get_axis_count
#skip	hb_ot_var_get_named_instance_count
#skip	hb_ot_var_has_data
#skip	hb_ot_var_named_instance_get_postscript_name_id
#skip	hb_ot_var_named_instance_get_subfamily_name_id

onstack	hb_color_stop_t
onstack	hb_font_extents_t
onstack	hb_glyph_extents_t
onstack	hb_feature_t
onstack	hb_variation_t

# Pango
out	pango_get_mirror_char		mirrored_ch
optional pango_get_mirror_char		mirrored_ch
skip	pango_log2vis_get_embedding_levels
onstack	PangoAttribute
onstack	PangoColor
onstack	PangoLogAttr
opaque	PangoLogAttr
onstack	PangoGlyphVisAttr
opaque	PangoGlyphVisAttr
onstack	PangoGlyphInfo
onstack	PangoGlyphGeometry
onstack	PangoRectangle
onstack	PangoItem
onstack	PangoAnalysis

onstack	PangoAttrColor
onstack	PangoAttrInt
onstack	PangoAttrShape
onstack	PangoAttrFloat
onstack	PangoAttrFontDesc
onstack	PangoAttrFontFeatures
onstack	PangoAttrLanguage
onstack	PangoAttrSize
opaque	PangoAttrSize
onstack	PangoAttrString

in	pango_font_get_features		len
in	PangoFont::vfunc_get_features	len

unowned	pango_script_iter_get_range	start
unowned	pango_script_iter_get_range	end
unowned	pango_scan_int			pos
unowned	pango_scan_string		pos
unowned	pango_scan_word			pos
unowned	pango_skip_space		pos
owned	pango_glyph_item_apply_attrs	glyph_item

# TODO: conflicts with a field, rename the field
skip	pango_layout_line_is_paragraph_start

nest	PangoLayoutSerializeFlags	Layout::SerializeFlags
nest	PangoLayoutDeserializeFlags	Layout::DeserializeFlags

nest	PangoLayoutIter			Layout::Iter
nest	PangoScriptIter			Script::Iter
nest	PangoGlyphItemIter		GlyphItem::Iter

# Graphene
onstack	graphene_rect_t
onstack	graphene_point_t
onstack	graphene_point3d_t
onstack	graphene_matrix_t
onstack	graphene_size_t
onstack	graphene_vec2_t
onstack	graphene_vec3_t
onstack	graphene_vec4_t
onstack	graphene_box_t
onstack	graphene_plane_t
onstack	graphene_sphere_t
onstack	graphene_quaternion_t
onstack	graphene_quad_t
onstack	graphene_ray_t
onstack	graphene_euler_t
onstack	graphene_simd4f_t
opaque	graphene_simd4f_t
onstack	graphene_simd4x4f_t
opaque	graphene_simd4x4f_t

# GdkPixbuf
skip	gdk_pixbuf_read_pixels
ifdef	GdkPixbufNonAnim		GDK_PIXBUF_ENABLE_BACKEND
opaque-ifndef	GdkPixbufFormat		GDK_PIXBUF_ENABLE_BACKEND
opaque-ifndef	GdkPixbufModule		GDK_PIXBUF_ENABLE_BACKEND
opaque-ifndef	GdkPixbufModulePattern	GDK_PIXBUF_ENABLE_BACKEND
opaque-ifndef	GdkPixbufAnimation	GDK_PIXBUF_ENABLE_BACKEND
opaque-ifndef	GdkPixbufAnimationIter	GDK_PIXBUF_ENABLE_BACKEND

# Gdk
skip	gdk_pango_layout_get_clip_region
skip	gdk_content_register_serializer
skip	gdk_content_register_deserializer
typed	gdk_clipboard_set		gdk_clipboard_set_value
typed	gdk_content_provider_new_typed	gdk_content_provider_new_for_value

nest	GdkDevicePadFeature		DevicePad::Feature
nest	GdkDeviceToolType		DeviceTool::Type
nest	GdkDragCancelReason		Drag::CancelReason
nest	GdkEventType			Event::Type

nest	GdkDragAction			Drag::Action
nest	GdkFrameClockPhase		FrameClock::Phase
nest	GdkPaintableFlags		Paintable::Flags
nest	GdkSeatCapabilities		Seat::Capabilities
nest	GdkToplevelState		Toplevel::State

nest	GdkTextureError			Texture::Error

nest	GdkToplevelSize			Toplevel::Size
nest	GdkToplevelLayout		Toplevel::Layout
nest	GdkDragSurfaceSize		DragSurface::Size

nest	GdkContentFormatsBuilder	ContentFormats::Builder

pointer	GdkToplevelSize
pointer	GdkDragSurfaceSize
onstack	GdkRGBA
onstack	GdkRectangle
onstack	GdkTimeCoord

# GdkX11
skip	gdk_x11_display_text_property_to_text_list
skip	gdk_x11_free_text_list
skip	gdk_x11_free_compound_text

# Gsk
#ifdef	GskBroadwayRenderer		GDK_WINDOWING_BROADWAY
skip	GskBroadwayRenderer

nest	GskPathDirection		Path::Direction
nest	GskPathOperation		Path::Operation
nest	GskPathForeachFlags		Path::ForeachFlags
nest	GskRenderNodeType		RenderNode::Type
nest	GskTransformCategory		Transform::Category

nest	GskPathPoint			Path::Point
opaque	GskPathPoint

onstack	GskColorStop
onstack	GskRoundedRect
onstack	GskPathPoint
onstack	GskShadow

take-value GskRenderNode		gsk_value_take_render_node

# Gtk
skip	gtk_tree_model_rows_reordered
skip	GtkTreeModel::vfunc_rows_reordered
skip	gtk_tree_path_get_indices

# These ones are bound as Traits<Gtk::Expression>
skip	gtk_value_dup_expression
skip	gtk_value_get_expression
skip	gtk_value_set_expression
skip	gtk_value_take_expression
take-value GtkExpression		gtk_value_take_expression

skip	GtkPageSetupUnixDialog
skip	GtkPrintJob
skip	GtkPrintCapabilities
skip	GtkPrintUnixDialog
skip	GtkPrinter
skip	gtk_enumerate_printers

out	GtkWidget::vfunc_compute_expand	hexpand_p
out	GtkWidget::vfunc_compute_expand	vexpand_p
out	GtkEntryBuffer::vfunc_get_text	n_bytes
nonnull	gtk_bitset_ref			self
nonnull	gtk_bitset_unref		self

vararg	gtk_snapshot_push_debug		format
vararg	gtk_media_stream_error		format
vararg	gtk_show_about_dialog		object-new
vararg	gtk_text_buffer_create_tag	object-new
vararg	gtk_shortcut_new_with_arguments	variant-new
vararg	gtk_widget_activate_action	variant-new
vararg	gtk_widget_class_add_binding	variant-new
vararg	gtk_widget_class_add_binding_action	variant-new
vararg	gtk_widget_class_add_binding_signal	variant-new
vararg	gtk_actionable_set_action_target	variant-new

vararg	gtk_message_dialog_new				format
vararg	gtk_message_dialog_new_with_markup		format
vararg	gtk_message_dialog_format_secondary_markup	format
vararg	gtk_message_dialog_format_secondary_text	format
vararg	gtk_alert_dialog_new				format

onstack	GtkRequestedSize
onstack	GtkRequisition
onstack	GtkAllocation
onstack	GtkTreeIter
onstack	GtkTextIter
onstack	GtkPadActionEntry
nest	GtkBitsetIter			Bitset::Iter
onstack	GtkBitsetIter
onstack	GtkBorder
onstack	GtkBuildableParser
nest	GtkBuildableParser		Buildable::Parser
nest	GtkBuildableParseContext	Buildable::ParseContext

cpp-wrapper GtkShortcutFunc
cpp-wrapper GtkWidgetActionActivateFunc

protected gtk_media_stream_prepared
protected gtk_media_stream_unprepared
protected gtk_media_stream_update
protected gtk_media_stream_ended
protected gtk_media_stream_seek_success
protected gtk_media_stream_seek_failed
protected gtk_media_stream_gerror
protected gtk_media_stream_error
protected gtk_media_stream_error_valist

protected gtk_widget_class_add_binding
protected gtk_widget_class_add_binding_action
protected gtk_widget_class_add_binding_signal
protected gtk_widget_class_add_shortcut
protected gtk_widget_class_bind_template_callback_full
protected gtk_widget_class_bind_template_child_full
protected gtk_widget_class_install_action
protected gtk_widget_class_install_property_action
protected gtk_widget_class_set_accessible_role
protected gtk_widget_class_set_activate_signal
protected gtk_widget_class_set_activate_signal_from_name
protected gtk_widget_class_set_css_name
protected gtk_widget_class_set_layout_manager_type
protected gtk_widget_class_set_template
protected gtk_widget_class_set_template_from_resource
protected gtk_widget_class_set_template_scope

protected gtk_widget_init_template
protected gtk_widget_dispose_template
protected gtk_widget_get_template_child
protected gtk_widget_action_set_enabled
protected gtk_widget_queue_allocate
protected gtk_widget_queue_draw
protected gtk_widget_queue_resize
protected gtk_widget_set_state_flags
protected gtk_widget_unset_state_flags
protected gtk_widget_snapshot_child

protected gtk_widget_set_parent
protected gtk_widget_insert_after
protected gtk_widget_insert_before
this	gtk_widget_set_parent		parent
this	gtk_widget_insert_after		parent
this	gtk_widget_insert_before	parent
float	gtk_widget_set_parent		widget
float	gtk_widget_insert_after		widget
float	gtk_widget_insert_before	widget

float	gtk_window_set_child		child
float	gtk_window_set_titlebar		titlebar
float	gtk_box_append			child
float	gtk_box_prepend			child
float	gtk_box_insert_child_after	child
float	gtk_list_box_set_adjustment	adjustment
float	gtk_list_box_row_set_header	header
float	gtk_tree_view_insert_column	column
float	gtk_spin_button_configure	adjustment
float	gtk_cell_area_box_pack_start	renderer
float	gtk_cell_area_box_pack_end	renderer
float	gtk_column_view_cell_set_child	child
float	gtk_list_header_set_child	child
float	gtk_list_item_set_child		child
float	gtk_window_handle_set_child	child
float	gtk_text_view_child_add		widget
float	gtk_graphics_offload_set_child	child
float	gtk_column_view_row_widget_add_child child
float	gtk_aspect_frame_set_child	child
float	gtk_flow_box_child_set_child	child
float	gtk_stack_add_titled		child
float	gtk_stack_add_child		child
float	gtk_stack_add_named		child
float	gtk_popover_set_child		child
float	gtk_center_box_set_start_widget	child
float	gtk_center_box_set_center_widget child
float	gtk_center_box_set_end_widget	child
float	gtk_header_bar_pack_start	child
float	gtk_header_bar_pack_end		child
float	gtk_header_bar_set_title_widget	title_widget
float	gtk_grid_attach			child
float	gtk_grid_attach_next_to		child
float	gtk_fixed_put			widget
float	gtk_button_set_child		child
float	gtk_viewport_set_child		child
float	gtk_menu_button_set_child	child
float	gtk_menu_button_set_popover	popover
float	gtk_revealer_set_child		child
float	gtk_frame_set_label_widget	label_widget
float	gtk_frame_set_child		child
float	gtk_tree_expander_set_child	child
float	gtk_scrolled_window_set_child	child
float	gtk_range_set_adjustment	adjustment
float	gtk_scrollbar_set_adjustment	adjustment
float	gtk_scrolled_window_set_hadjustment hadjustment
float	gtk_scrolled_window_set_vadjustment vadjustment

unowned	GtkWindow			construct
typed	gtk_constant_expression_new	gtk_constant_expression_new_for_value

float	gtk_actionable_set_action_target_value		target_value
float	GtkActionable::vfunc_set_action_target_value	target_value
float	gtk_shortcut_set_arguments	args

nest	GtkAccessibleAnnouncementPriority Accessible::AnnouncementPriority
nest	GtkAccessibleAutocomplete	Accessible::Autocomplete
nest	GtkAccessibleInvalidState	Accessible::InvalidState
nest	GtkAccessiblePlatformState	Accessible::PlatformState
nest	GtkAccessibleProperty		Accessible::Property
nest	GtkAccessibleRelation		Accessible::Relation
nest	GtkAccessibleRole		Accessible::Role
nest	GtkAccessibleSort		Accessible::Sort
nest	GtkAccessibleState		Accessible::State
nest	GtkAccessibleTristate		Accessible::Tristate

nest	GtkAccessibleTextContentChange	AccessibleText::ContentChange
nest	GtkAccessibleTextGranularity	AccessibleText::Granularity
nest	GtkAccessibleTextRange		AccessibleText::Range
onstack	GtkAccessibleTextRange

nest	GtkAssistantPageType		Assistant::PageType
nest	GtkCellRendererAccelMode	CellRendererAccel::Mode
nest	GtkCellRendererMode		CellRenderer::Mode
nest	GtkEditableProperties		Editable::Properties
nest	GtkEntryIconPosition		Entry::IconPosition
# EventSequence is in GDK, so no nest here
#nest	GtkEventSequenceState		EventSequence::State
nest	GtkFileChooserAction		FileChooser::Action
nest	GtkFilterChange			Filter::Change
nest	GtkFilterMatch			Filter::Match
nest	GtkGraphicsOffloadEnabled	GraphicsOffload::Enabled
nest	GtkIconViewDropPosition		IconView::DropPosition
nest	GtkImageType			Image::Type
nest	GtkInscriptionOverflow		Inscription::Overflow
nest	GtkLevelBarMode			LevelBar::Mode
nest	GtkNotebookTab			Notebook::Tab
nest	GtkPrintOperationAction		PrintOperation::Action
nest	GtkPrintOperationResult		PrintOperation::Result
nest	GtkRevealerTransitionType	Revealer::TransitionType
nest	GtkScrollablePolicy		Scrollable::Policy
nest	GtkShortcutScope		Shortcut::Scope
nest	GtkSizeGroupMode		SizeGroup::Mode
nest	GtkSorterChange			Sorter::Change
nest	GtkSorterOrder			Sorter::Order
nest	GtkSpinButtonUpdatePolicy	SpinButton::UpdatePolicy
nest	GtkStackTransitionType		Stack::TransitionType
nest	GtkStringFilterMatchMode	StringFilter::MatchMode
nest	GtkTextViewLayer		TextView::Layer

nest	GtkTreeViewColumnSizing		TreeView::ColumnSizing
nest	GtkTreeViewDropPosition		TreeView::DropPosition
nest	GtkTreeViewGridLines		TreeView::GridLines

nest	GtkConstraintAttribute		Constraint::Attribute
nest	GtkConstraintRelation		Constraint::Relation
nest	GtkConstraintStrength		Constraint::Strength

nest	GtkApplicationInhibitFlags	Application::InhibitFlags
nest	GtkBuilderClosureFlags		Builder::ClosureFlags
nest	GtkCellRendererState		CellRenderer::State
nest	GtkDialogFlags			Dialog::Flags
nest	GtkEventControllerScrollFlags	EventControllerScroll::Flags
nest	GtkFontChooserLevel		FontChooser::Level
nest	GtkPopoverMenuFlags		PopoverMenu::Flags
nest	GtkShortcutActionFlags		ShortcutAction::Flags
nest	GtkStyleContextPrintFlags	StyleContext::PrintFlags
nest	GtkTreeModelFlags		TreeModel::Flags
nest	GtkTextBufferNotifyFlags	TextBuffer::NotifyFlags

nest	GtkBuilderError			Builder::Error
nest	GtkConstraintVflParserError	Constraint::VflParserError
nest	GtkFileChooserError		FileChooser::Error
nest	GtkIconThemeError		IconTheme::Error
nest	GtkRecentManagerError		RecentManager::Error

nest	GtkConstraintTarget		Constraint::Target

nest	GtkColumnViewColumn		ColumnView::Column
nest	GtkColumnViewRow		ColumnView::Row
nest	GtkColumnViewCell		ColumnView::Cell
nest	GtkColumnViewSorter		ColumnView::Sorter

nest	GtkBuilderScope			Builder::Scope
nest	GtkBuilderCScope		Builder::CScope
nest	GtkCellAreaContext		CellArea::Context

no-autobase GtkBuildable
no-autobase GtkAccessible
prefer-autobase GtkSectionModel		SelectionModel

prefer-autobase GtkTreeDragSource	TreeModel
prefer-autobase GtkCellEditable		Editable

# Adw
float	adw_window_set_content		content
float	adw_application_window_set_content content
float	adw_header_bar_set_title_widget	title_widget
float	adw_header_bar_pack_start	child
float	adw_header_bar_pack_end		child
float	adw_toolbar_view_add_top_bar	widget
float	adw_toolbar_view_add_bottom_bar	widget
float	adw_toolbar_view_set_content	content
float	adw_tab_overview_set_child	child
float	adw_tab_view_add_page		child
float	adw_tab_view_append		child
float	adw_tab_view_append_pinned	child
float	adw_tab_view_insert		child
float	adw_tab_view_insert_pinned	child
float	adw_tab_view_prepend		child
float	adw_tab_view_prepend_pinned	child

vararg	adw_show_about_dialog		object-new
vararg	adw_show_about_dialog_from_appdata object-new
vararg	adw_show_about_window		object-new
vararg	adw_show_about_window_from_appdata object-new
vararg	adw_breakpoint_add_setters	object-new
vararg	adw_toast_set_action_target	variant-new
float	adw_toast_set_action_target_value action_target

nest	AdwBreakpointCondition		Breakpoint::Condition
nest	AdwBreakpointConditionLengthType Breakpoint::Condition::LengthType
nest	AdwBreakpointConditionRatioType	Breakpoint::Condition::RatioType

nest	AdwAnimationState		Animation::State
nest	AdwAnimationTarget		Animation::Target
nest	AdwViewSwitcherPolicy		ViewSwitcher::Policy
nest	AdwToastPriority		Toast::Priority
nest	AdwDialogPresentationMode	Dialog::PresentationMode
nest	AdwFlapFoldPolicy		Flap::FoldPolicy
nest	AdwFlapTransitionType		Flap::TransitionType
nest	AdwSqueezerTransitionType	Squeezer::TransitionType
nest	AdwLeafletTransitionType	Leaflet::TransitionType
nest	AdwTabViewShortcuts		TabView::Shortcuts

skip	adw_breakpoint_add_settersv
skip	AdwSwipeable::vfunc_get_snap_points

hide	AdwWindow			set_child
hide	AdwWindow			get_child
hide	AdwWindow			set_titlebar
hide	AdwWindow			get_titlebar
hide	AdwWindow			prop_child
hide	AdwApplicationWindow		set_child
hide	AdwApplicationWindow		get_child
hide	AdwApplicationWindow		set_titlebar
hide	AdwApplicationWindow		get_titlebar
hide	AdwApplicationWindow		prop_child

# Gee
opaque	GeeHazardPointer

# Dex
take-value DexObject			dex_value_take_object
skip	dex_value_get_object
skip	dex_value_set_object
skip	dex_value_take_object
nest	DexFutureStatus			Future::Status

# Soup
nest	SoupMessagePriority		Message::Priority
nest	SoupMessageHeadersType		MessageHeaders::Type
nest	SoupCookieJarAcceptPolicy	CookieJar::AcceptPolicy
nest	SoupCacheType			Cache::Type
nest	SoupSessionFeature		Session::Feature
nest	SoupLoggerLogLevel		Logger::LogLevel
vararg	soup_session_new_with_options	object-new

# Gst
in	gst_buffer_extract		size
skip	gst_mini_object_weak_ref
skip	gst_mini_object_weak_unref
skip	gst_pad_set_activate_function_full
skip	gst_pad_set_activatemode_function_full
skip	gst_pad_set_chain_function_full
skip	gst_pad_set_chain_list_function_full
skip	gst_pad_set_getrange_function_full
skip	gst_pad_set_event_function_full
skip	gst_pad_set_event_full_function_full
skip	gst_pad_set_iterate_internal_links_function_full
skip	gst_pad_set_link_function_full
skip	gst_pad_set_query_function_full
skip	gst_pad_set_unlink_function_full
scope	gst_bus_add_watch		func	forever

# Json (json-glib)
no-autofree JsonNode
nest	JsonNodeType			Node::Type
nest	JsonObjectIter			Object::Iter

# GtkSource
vararg	gtk_source_buffer_create_source_tag	object-new
